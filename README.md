Elastic BeanStalk Node.js Sample!



A simple Node.js application with tests that will be deployed to [Amazon Elastic BeanStalk][1]

Uses Grunt to run tests against an Express server, then generates reports with Xunit and Istanbul.

This sample is built for Shippable, a docker based continuous integration and deployment platform.

[1]: https://aws.amazon.com/elasticbeanstalk/
[![Run Status](https://apibeta.shippable.com/projects/57024658c77dae78a8fcc713/badge?branch=master)](https://beta.shippable.com/projects/57024658c77dae78a8fcc713)
